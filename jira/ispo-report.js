$('html > body')
	.empty()
	.attr("class", "")
	.css("overflow-y", "visible")
	.append(`
	<table id='menu'>
		<tr>
		  <td class="noselect">
			<select id="section">
			  <option value="CLEAR">-= CLEAR =-</option>
			  <option value="VA">VA</option>
			  <option value="HPCS">HPCS</option>
			  <option value="KHPR">KHPR</option>
			</select>
		  </td>
		  <td class="noselect"><label class="total" onclick="filterOff()">total<span id="count_total"></span></label></td>
		  <td class="noselect"><label class="toDo"><input id="toDo" checked="checked" type="checkbox" onclick="filter(this)"/>toDo<span id="toDo"></label></td>
		  <td class="noselect"><label class="inProgress"><input id="inProgress" checked="checked" type="checkbox" onclick="filter(this)"/>inProgress<span id="inProgress"></label></td>
		  <td class="noselect"><label class="forTest"><input id="forTest" checked="checked" type="checkbox" onclick="filter(this)"/>forTest<span id="forTest"></label></td>

		  <td class="noselect"><label class="done"><input id="done" checked="checked" type="checkbox" onclick="filter(this)"/>done<span id="count_done"></label></td>
		  <td class="noselect"><label class="unknown"><input id="unknown" checked="checked" type="checkbox" onclick="filter(this)"/>unknown<span id="count_unknown"></label></td>
		  <td class="noselect"><label class="nonExistent"><input id="nonExistent" checked="checked" type="checkbox" onclick="filter(this)"/>nonExistent<span id="count_nonExistent"></label></td>
		  
			<td class="noselect"><label class="nonExistent" onclick="openInNewTab('https://gitlab.com/pavolduda/cjs/-/edit/main/jira/smart-report.js')">edit</label></td>
			<td class="noselect"><label class="nonExistent" onclick="reorder()">reorder</label></td>
			<td class="noselect"><label class="nonExistent" onclick="toggle()">toggle</label></td>
		</tr>
	</table>
	<table id='result'>
	  <thead>
		<tr>
		  <th>Epic</th>
		  <th>Task</th>
		  <th>Components</th>
		  <th>Type</th>
		  <th>Title</th>
		  <th>Sprint</th>
		  <th><div>Estimation</div> [<span id="count_estimation">0</span>]</th>
		  <th>TimeEstimated</th>
		  <th>TimeRemaining</th>
		  <th><div>TimeLogged</div> [<span id="count_timeLogged">0</span>]</th>
		  <th>Status</th>
		  <th>Assignee</th>
		</tr>
	  </thead>
	</table>
`);

function process(jiraTask) {
	$("#" + jiraTask)
		.attr('class', "loading")
		.attr('style', "")
		.html(
			"<td colspan='12'>loading: " + jiraTask + "</td>"
		);
	$.ajax({
		//url: jiraHome + "/browse/" + jiraTask,
		url: "https://ispo.atlassian.net/rest/graphql/1/",
		dataType : "json",
    	contentType: "application/json; charset=utf-8",
		data: '{"query":"query {\n        issue(issueIdOrKey: \"ISPO-473\", latestVersion: true, screen: \"view\") {\n            id\n            viewScreenId \n            fields {\n                key\n                title\n                editable\n                required\n                autoCompleteUrl\n                allowedValues\n                content\n                renderedContent\n                schema {\n                    custom\n                    system\n                    configuration {\n        key\n        value\n    }\n    \n                    items\n                    type\n                    renderer\n                }\n                configuration\n            }\n            expandAssigneeInSubtasks\n            expandAssigneeInIssuelinks\n            expandTimeTrackingInSubtasks\n            systemFields {\n                descriptionAdf {\n                    value\n                }\n                environmentAdf {\n        value\n    }\n            }\n            customFields {\n                textareaAdf {\n                    key\n                    value\n                }\n            }            \n            tabs {\n        id\n        name\n        items {\n            id\n            type\n        }\n    }\n            \n    isHybridAgilityProject\n    \n            \n    agile {\n        epic {\n          key\n        },\n    }\n        }\n        \n        project(projectIdOrKey: \"ISPO\") {\n            id\n            name\n            key\n            projectTypeKey\n            simplified\n            avatarUrls {\n                key\n                value\n            }\n            archived\n            deleted\n        }\n    }"}',
		type: 'post',
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(XMLHttpRequest);
			console.log(textStatus);
			console.log(errorThrown);
			var category = "nonExistent";

			var trStyle = "";
			if (!isCategoryVisible(category)) {
				trStyle = "display:none";
			}
			$("#" + jiraTask)
				//.attr('class', category)
				.attr('class', "")
				.attr("data-category", category)
				.attr("data-type", "None")
				.attr("data-index", "99999999")
				.attr('style', trStyle)
				.html(
					"<td colspan='12'>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + jiraTask + "'>" + jiraTask + "</a></td>"
					/*"<td>"+task+"</td>"+
					
					"<td>"+componentsLink+"</td>"+
					"<td>"+typeIcon+"</td>"+
					"<td>"+title+"</td>"+
					"<td>"+statusIcon+"</td>"+
					"<td>"+assigneeIcon+"</td>"+*/
				);
			updateCount(category);
		},
		success: function (page) {
			$page = $(page);
			console.log($page.find("#ak-main-content"));
			console.log($page.find("#ak-main-content h1").text());
			console.log(page);
			title = $page.find("#ak-main-content h1")[0].innerHTML;
			task = $page.find("#key-val")[0].innerHTML;
			status = $page.find("#status-val > span")[0].innerHTML;
			statusIcon = $page.find("#status-val")[0].innerHTML;
			type = $page.find("#type-val")[0].innerText.trim(); 1
			typeIcon = $page.find("#type-val")[0].innerHTML;
			assignee = $page.find("#assignee-val span")[0].innerText.trim();
			assigneeIcon = $page.find("#assignee-val")[0].innerHTML;
			estimation = "";
			try {
				estimation = $page.find("#customfield_10100-val")[0].innerText.trim();
			} catch (error) { }
			timeEstimated = "";
			try {
				timeEstimated = $page.find("#tt_single_text_orig").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			timeRemaining = "";
			try {
				timeRemaining = $page.find("#tt_single_text_remain").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			timeLogged = "";
			try {
				timeLogged = $page.find("#tt_single_text_spent").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			componentsLink = "N/A";
			try {
				componentsLink = $page.find("#components-val")[0].innerHTML;
			} catch (error) { }
			epic = "";
			try {
				epic = $page.find(".type-gh-epic-link a").attr("href").replace('/browse/','')
			} catch (error) { }
			if(type == "Epic"){
				epic = task;
			}
			epicNo = 0;
			try {
				if(type == "Epic"){
					epicNo = parseInt($page.find(".type-gh-epic-label")[0].innerText.trim())
				}
				else {
					epicNo = parseInt($page.find(".type-gh-epic-link a")[0].innerText.trim())
				}
			} catch (error) { }
			epicNo = epicNo | 0;
			if(type == "Epic"){
				epic = task;
			}
			sprint = "";
			try {
				sprint = $page.find("#customfield_10004-val")[0].innerText.trim().replace(/,/g, "<br/>");
			} catch (error) { }
			console.log(jiraTask + "\t" + task + "\t" + type + "\t" + status + "\t" + assignee);
			var category = "unknown";
			if (status == 'To Do') {
				category = "toDo"
			}
			if (status == 'In Progress') {
				category = "inProgress"
			}
			if (status == 'READY FOR QA TEST') {
				category = "forTest"
			}
			if (type == 'Development' && (status != 'Developed')) {
				category = "inProgressBE"
			}
			if (status == 'Done') {
				category = "done"
			}
			var trStyle = "";
			if (!isCategoryVisible(category)) {
				trStyle = "display:none";
			}
			var index = epic == task ?
						epicNo*1000000 + calcId(epic)*1000
						:
						epicNo*1000000 + calcId(epic)*1000 + calcId(task);
			$("#" + jiraTask)
				//.attr('class', category+" "+type)
				.attr('class', "")
				.attr('style', trStyle)
				.attr("data-type", type)
				.attr("data-category", category)
				.attr("data-index", index)
				.attr("data-epic", epic)
				.attr("data-task", task)
				.html(
					"<td>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + epic + "'>" + epic + "</a>"
					+ "</td>" +
					"<td>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + jiraTask + "'>" + jiraTask + "</a><br>" +
					(jiraTask != task ? task : '')
					+ "</td>" +
					"<td>" + componentsLink + "</td>" +
					"<td>" + typeIcon + "</td>" +
					"<td>" + title + "</td>" +
					"<td>" + sprint + "</td>" +
					"<td data-value='estimation'>" + minToJiraTime(jiraTimeToMin(estimation)) + "</td>" +
					"<td>" + timeEstimated + "</td>" +
					"<td>" + timeRemaining + "</td>" +
					"<td data-value='timeLogged'>" + timeLogged + "</td>" +
					"<td>" + statusIcon + "</td>" +
					"<td>" + assigneeIcon + "</td>"
				);
			updateCount(category);
		}
	});
}
function isCategoryVisible(category) {
	var $element = $("#" + category);
	//console.log($element);
	return $element.prop('checked');
}


	var jiraArray = jira.split("\n");
	jiraArray = jiraArray.filter(function (el) { return el != ""; });

	jiraArray.forEach(myFunction);

	function myFunction(item, index) {
	  console.log("a", item, index);
		$("#result").append("<tr id='" + item + "' onclick='process(\"" + item + "\")'></tr>");
		process(item);
	}
