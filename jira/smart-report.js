//eval("1w 2d 4h 38m".replace("w","*7*24*60+").replace("d","*24*60+").replace("h","*60+").replace("m","*1+")+"0")
//var jira2=`
/*var head = document.getElementsByTagName('head')[0],
		script;
	
	//delete window.$; delete window.jQuery;
	
	script = document.createElement('script')
	script.setAttribute("type", "text/javascript");
	script.setAttribute("src", 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js');
	head.appendChild(script);
	*/


var styleTag = $(`<style>
	#menu {
		margin: 10px;
	}
	
	#menu td{
		vertical-align: top;
	}
	
	#menu select{
		height: 42px;
	}
	
	#menu table, #menu th, #menu td {border: 0px}
	
	#menu label {
	 border:1px solid #ccc;
	 padding:10px;
	 margin:0 0 10px;
	 display:block; 
	}
	#menu label span:before {
	  content: " [";
	}
	#menu label span:after {
	  content: "]";
	}

	#menu label:hover {
	 background:#eee;
	 cursor:pointer;
	}	
	
	#result {border-collapse:collapse; width:100%}
	
	#result table, #result th, #result td {  border: 1px solid black;}
	
	.noselect {
	  -webkit-touch-callout: none; /* iOS Safari */
		-webkit-user-select: none; /* Safari */
		 -khtml-user-select: none; /* Konqueror HTML */
		   -moz-user-select: none; /* Firefox */
			-ms-user-select: none; /* Internet Explorer/Edge */
				user-select: none; /* Non-prefixed version, currently
									  supported by Chrome and Opera */
	}
	.loading { background: #333333; color: lightgrey; height: 28px }
	[data-category="toDo"] { background: #2196F3 }
	[data-category="inProgress"] { background: #FFEB3B }
	[data-category="forTest"] { background: #FF9800 }
    .xxinProgressBE { background: #00aeef }
	.xxinProgressPMO { background: #2196F3 }
	xxx[data-category="waitingForOther"] { background: #faa61a }
	xxx[data-category="almostDone"] { background: #8BC34A }
	[data-category="done"] { background: #71bf44 }
	[data-category="unknown"] { background: #cccccc }
	[data-category="nonExistent"] { background: #222222; color: lightgrey; }


	[data-type="Epic"] {
    	font-weight: bold;
		font-size: 15px;
	}
	
	#result td {
		vertical-align: top;
		overflow:hidden;
		white-space:nowrap
	}
	
	#result td:nth-child(3) {
	  width: 200px;
	  white-space: unset;
	}
	.aui-avatar-small .aui-avatar-inner img {
		max-height: 24px;
		max-width: 24px;
	}
	#result tr:not([data-type="Epic"]) td:nth-child(1) {
		font-size: x-small;
		padding: 6px 0px 0 12px;
		opacity: 0.3;
	}
	#result tr[data-type="Epic"] td:nth-child(2) {
		font-size: x-small;
		padding: 6px 0px 0 12px;
		opacity: 0.3;
	}
</style>`)

document.title = "JIRA report";
//var jiraHome = "https://jira.ferratum.com/jira";
var jiraHome = "https://jira.scr.sk";

function jiraTimeToMin(time){
    var wMin = "*5*8*60+",
        dMin = "*8*60+",
        hMin = "*60+",
        mMin = "*1+",
        formula = time.replaceAll("w",wMin).replaceAll("d",dMin).replaceAll("h",hMin).replaceAll("m",mMin);
    if(formula.endsWith("+")) { formula += "0"; }
	if(formula.trim() == "") { return 0; }
	try {
    	return eval(formula);
	} catch (error) { console.error("Error formula: '"+formula+"'"); }	
	return 0;
}

function minToJiraTime(min){
    var wMin = 5*8*60,
        dMin = 8*60,
        hMin = 60,
        mMin = 1,
        w = Math.trunc(min / wMin)
        d = Math.trunc((min - w*wMin) / dMin)
        h = Math.trunc((min - w*wMin - d*dMin) / hMin)
        m = Math.trunc((min - w*wMin - d*dMin - h*hMin) / mMin)
        result = "";
    if(w) { result += w+"w "; }
    if(d) { result += d+"d "; }
    if(h) { result += h+"h "; }
    if(m) { result += m+"m"; }
    return result.trim();
}



function calcId(key){
    if(!key){ return 0 }
    var index = key.indexOf("-");
    if(index){
        return parseInt(key.substr(index+1))
    }
}

function reorder(){
	var $table=$('#result ');

	var rows = $table.find('tbody tr').get();
	function calcId(key){
		if(!key){ return 0 }
		var index = key.indexOf("-");
		if(index){
			return parseInt(key.substr(index+1))
		}
	}
	rows.sort(function(a, b) {
		var indexA = calcId($(a).attr('data-index'));
		var indexB = calcId($(b).attr('data-index'));
		if (indexA > indexB) return 1;
		if (indexA < indexB) return -1;
		return 0;
	});
	$.each(rows, function(index, row) {
		$table.children('tbody').append(row);
	});
}

function toggle(){
	$("#result tbody tr:not([data-type='Epic'])").toggle();
}

function process(jiraTask) {
	$("#" + jiraTask)
		.attr('class', "loading")
		.attr('style', "")
		.html(
			"<td colspan='12'>loading: " + jiraTask + "</td>"
		);
	$.ajax({
		url: jiraHome + "/browse/" + jiraTask,
		type: 'get',
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(XMLHttpRequest);
			console.log(textStatus);
			console.log(errorThrown);
			var category = "nonExistent";

			var trStyle = "";
			if (!isCategoryVisible(category)) {
				trStyle = "display:none";
			}
			$("#" + jiraTask)
				//.attr('class', category)
				.attr('class', "")
				.attr("data-category", category)
				.attr("data-type", "None")
				.attr("data-index", "99999999")
				.attr('style', trStyle)
				.html(
					"<td colspan='12'>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + jiraTask + "'>" + jiraTask + "</a></td>"
					/*"<td>"+task+"</td>"+
					
					"<td>"+componentsLink+"</td>"+
					"<td>"+typeIcon+"</td>"+
					"<td>"+title+"</td>"+
					"<td>"+statusIcon+"</td>"+
					"<td>"+assigneeIcon+"</td>"+*/
				);
			updateCount(category);
		},
		success: function (page) {
			$page = $(page);
			title = $page.find("#summary-val")[0].innerHTML;
			task = $page.find("#key-val")[0].innerHTML;
			status = $page.find("#status-val > span")[0].innerHTML;
			statusIcon = $page.find("#status-val")[0].innerHTML;
			type = $page.find("#type-val")[0].innerText.trim(); 1
			typeIcon = $page.find("#type-val")[0].innerHTML;
			assignee = $page.find("#assignee-val span")[0].innerText.trim();
			assigneeIcon = $page.find("#assignee-val")[0].innerHTML;
			estimation = "";
			try {
				estimation = $page.find("#customfield_10100-val")[0].innerText.trim();
			} catch (error) { }
			timeEstimated = "";
			try {
				timeEstimated = $page.find("#tt_single_text_orig").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			timeRemaining = "";
			try {
				timeRemaining = $page.find("#tt_single_text_remain").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			timeLogged = "";
			try {
				timeLogged = $page.find("#tt_single_text_spent").parent().find(".tt_values")[0].innerText.trim()
			} catch (error) { }
			componentsLink = "N/A";
			try {
				componentsLink = $page.find("#components-val")[0].innerHTML;
			} catch (error) { }
			epic = "";
			try {
				epic = $page.find(".type-gh-epic-link a").attr("href").replace('/browse/','')
			} catch (error) { }
			if(type == "Epic"){
				epic = task;
			}
			epicNo = 0;
			try {
				if(type == "Epic"){
					epicNo = parseInt($page.find(".type-gh-epic-label")[0].innerText.trim())
				}
				else {
					epicNo = parseInt($page.find(".type-gh-epic-link a")[0].innerText.trim())
				}
			} catch (error) { }
			epicNo = epicNo | 0;
			if(type == "Epic"){
				epic = task;
			}
			sprint = "";
			try {
				sprint = $page.find("#customfield_10004-val")[0].innerText.trim().replace(/,/g, "<br/>");
			} catch (error) { }
			console.log(jiraTask + "\t" + task + "\t" + type + "\t" + status + "\t" + assignee);
			var category = "unknown";
			if (status == 'To Do') {
				category = "toDo"
			}
			if (status == 'In Progress') {
				category = "inProgress"
			}
			if (status == 'READY FOR QA TEST') {
				category = "forTest"
			}
			if (type == 'Development' && (status != 'Developed')) {
				category = "inProgressBE"
			}
			if (status == 'Done') {
				category = "done"
			}
			var trStyle = "";
			if (!isCategoryVisible(category)) {
				trStyle = "display:none";
			}
			var index = epic == task ?
						epicNo*1000000 + calcId(epic)*1000
						:
						epicNo*1000000 + calcId(epic)*1000 + calcId(task);
			$("#" + jiraTask)
				//.attr('class', category+" "+type)
				.attr('class', "")
				.attr('style', trStyle)
				.attr("data-type", type)
				.attr("data-category", category)
				.attr("data-index", index)
				.attr("data-epic", epic)
				.attr("data-task", task)
				.html(
					"<td>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + epic + "'>" + epic + "</a>"
					+ "</td>" +
					"<td>" + "<a target='_blank' rel='noopener noreferrer' href='"+jiraHome+"/browse/" + jiraTask + "'>" + jiraTask + "</a><br>" +
					(jiraTask != task ? task : '')
					+ "</td>" +
					"<td>" + componentsLink + "</td>" +
					"<td>" + typeIcon + "</td>" +
					"<td>" + title + "</td>" +
					"<td>" + sprint + "</td>" +
					"<td data-value='estimation'>" + minToJiraTime(jiraTimeToMin(estimation)) + "</td>" +
					"<td>" + timeEstimated + "</td>" +
					"<td>" + timeRemaining + "</td>" +
					"<td data-value='timeLogged'>" + timeLogged + "</td>" +
					"<td>" + statusIcon + "</td>" +
					"<td>" + assigneeIcon + "</td>"
				);
			updateCount(category);
		}
	});
}
var menu = {
	section: {
		VA: { min: 0, max: 200 },
		HPCS: { min: 0, max: 50 },
		KHPR: { min: 0, max: 50 },
		//HBT: { min: 0, max: 50 },
		//ASSR: { min: 0, max: 50 },
		//OCH: { min: 0, max: 100 },
		//PBD: { min: 0, max: 100 },
		//SRIA: { min: 0, max: 100 },
		//BIN410: { min: 1100, max: 1200 },
		//UI: { min: 0, max: 750 }
	}
};
function updateCount(category) {
	$("input").each(function () {
		var category = $(this).attr("id");
		var count = $("#result [data-category='"+category+"']").length;
		$("#count_" + category).text(count);
	});

	var all = $("#result tbody tr").length;
	var loading = $("#result .loading").length;
	var ready = all - loading;

	$("#count_total").text(ready + " of " + all);

	if (loading == 0) {
		$("#count_total").text(all);
		console.log("READY");
		
	}
	estimationCount = 0;
	timeLoggedCount = 0;
	//console.log("aaaaaaaaaaaaaaaaaaa")
	$("#result tbody td[data-value='estimation']").each(function(a, b){
		var value = b.innerText.trim(),
			min = jiraTimeToMin(value);
		estimationCount += min;
		//console.log(value, min, estimationCount)
	})
	$("#result tbody td[data-value='timeLogged']").each(function(a, b){
		var value = b.innerText.trim(),
			min = jiraTimeToMin(value);
		timeLoggedCount += min;
		//console.log(value, min, estimationCount)
	})
	//console.log("bbbbbbbbbbbbbbbbbbb", estimationCount)
//	console.log("estimationCount" + estimationCount);
	$("#count_estimation").text(minToJiraTime(estimationCount));
	$("#count_timeLogged").text(minToJiraTime(timeLoggedCount));


}
function filter(element) {
	console.log(element);
	var $element = $(element),
		id = $element.attr('id'),
		checked = $element.prop('checked');
	if (checked) {
		$("#result [data-category='"+id+"']").show();
		if (id == 'nonExistent') {
			$("#result [data-category='nonExistent']").each(function () {
				process($(this).attr("id"))
			});
		}
	}
	else {
		$("#result [data-category='"+id+"']").hide();
	}
}
function filterOff() {
	var checkedCount = $('input:checkbox:checked').length;
	if (checkedCount == 0) {
		$("#result tbody tr").show();
		$("input").attr('checked', 'checked');
	}
	else {
		$("#result tbody tr").hide();
		$("input").removeAttr('checked');
	}
}
function isCategoryVisible(category) {
	var $element = $("#" + category);
	//console.log($element);
	return $element.prop('checked');
}

$('html > head')
	.empty()
	.append($('<link rel="stylesheet" type="text/css" />').attr('href', jiraHome+'/s/fa5a941d92406b8dbedb9187ae3b36b2-CDN/5ax36e/74006/fa0d1d329130b80bcfcde028b799d04d/84bc7227d1cb5154b586d65ab1c07498/_/download/contextbatch/css/_super/batch.css'))
	.append(styleTag)

$(function () {
	var head = document.getElementsByTagName('head')[0],
		script;

	var scriptContent = `
if (!window.jQuery) {
	
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
    script.type = 'text/javascript';
    script.onload = function() {
        var $ = window.jQuery;
`+ "$('#section').prop('disabled', true);" + `
    };
    document.getElementsByTagName("head")[0].appendChild(script);
`+ filter.toString() + `
`+ filterOff.toString() + `
`+ "process = function(){}" + `
};	
	`;

	script = document.createElement('script')
	script.setAttribute("type", "text/javascript");
	script.appendChild(document.createTextNode(scriptContent));
	head.appendChild(script);

});

$('html > body')
	.empty()
	.attr("class", "")
	.css("overflow-y", "visible")
	.append(`
	<table id='menu'>
		<tr>
		  <td class="noselect">
			<select id="section">
			  <option value="CLEAR">-= CLEAR =-</option>
			  <option value="VA">VA</option>
			  <option value="HPCS">HPCS</option>
			  <option value="KHPR">KHPR</option>
			</select>
		  </td>
		  <td class="noselect"><label class="total" onclick="filterOff()">total<span id="count_total"></span></label></td>
		  <td class="noselect"><label class="toDo"><input id="toDo" checked="checked" type="checkbox" onclick="filter(this)"/>toDo<span id="toDo"></label></td>
		  <td class="noselect"><label class="inProgress"><input id="inProgress" checked="checked" type="checkbox" onclick="filter(this)"/>inProgress<span id="inProgress"></label></td>
		  <td class="noselect"><label class="forTest"><input id="forTest" checked="checked" type="checkbox" onclick="filter(this)"/>forTest<span id="forTest"></label></td>

		  <td class="noselect"><label class="done"><input id="done" checked="checked" type="checkbox" onclick="filter(this)"/>done<span id="count_done"></label></td>
		  <td class="noselect"><label class="unknown"><input id="unknown" checked="checked" type="checkbox" onclick="filter(this)"/>unknown<span id="count_unknown"></label></td>
		  <td class="noselect"><label class="nonExistent"><input id="nonExistent" checked="checked" type="checkbox" onclick="filter(this)"/>nonExistent<span id="count_nonExistent"></label></td>
		  
			<td class="noselect"><label class="nonExistent" onclick="openInNewTab('https://gitlab.com/pavolduda/cjs/-/edit/main/jira/smart-report.js')">edit</label></td>
			<td class="noselect"><label class="nonExistent" onclick="reorder()">reorder</label></td>
			<td class="noselect"><label class="nonExistent" onclick="toggle()">toggle</label></td>
		</tr>
	</table>
	<table id='result'>
	  <thead>
		<tr>
		  <th>Epic</th>
		  <th>Task</th>
		  <th>Components</th>
		  <th>Type</th>
		  <th>Title</th>
		  <th>Sprint</th>
		  <th><div>Estimation</div> [<span id="count_estimation">0</span>]</th>
		  <th>TimeEstimated</th>
		  <th>TimeRemaining</th>
		  <th><div>TimeLogged</div> [<span id="count_timeLogged">0</span>]</th>
		  <th>Status</th>
		  <th>Assignee</th>
		</tr>
	  </thead>
	</table>
`);

dateToString = x => (f = x => (x < 10 && '0') + x, x.getYear() - 100 + f(x.getMonth() + 1) + f(x.getDate()) + '-' + f(x.getHours()) + f(x.getMinutes()) + f(x.getSeconds()));

function openInNewTab(url) {
	var win = window.open(url, '_blank');
	win.focus();
}

$('#section').change(function () {
	var $item = $(this),
		val = $item.val();
	console.log(val);

	document.title = "JIRA: " + val + " @ " + dateToString(new Date());

	if (val == "CLEAR") {
		$("#result tbody").empty();
	}
	else {
		var i, id,
			min = menu.section[val].min,
			max = menu.section[val].max;

		$("#result tbody").empty();

		console.log(max);

		//		for (i = 1; i <= max; i++) { 
		for (i = max; i > min; i--) {
			id = val + "-" + i;
			$("#result").prepend("<tr id='" + id + "' onclick='process(\"" + id + "\")'></tr>");
			process(id);
		}
	}

});

function run() {

	var jira = `
BIN1186-386
BIN1186-387
`;



	var jiraArray = jira.split("\n");
	jiraArray = jiraArray.filter(function (el) { return el != ""; });

	jiraArray.forEach(myFunction);

	function myFunction(item, index) {
		$("#result").append("<tr id='" + item + "' onclick='process(\"" + item + "\")'></tr>");
		process(item);
	}



}
