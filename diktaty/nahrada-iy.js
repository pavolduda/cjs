//https://slovencina.eu/gramatika/diktaty/diktat-vybrane-slova/

var fill='<svg viewBox="0 0 12 16" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m10.998 4h-9.9982c-0.55219 0-1.0003 0.44812-1.0003 1.0003v9.9994c0 0.55219 0.44812 1.0003 1.0003 1.0003h9.9994c0.55219 0 1.0003-0.44812 1.0003-1.0003v-9.9994c-0.001292-0.55219-0.44934-1.0003-1.0015-1.0003zm0 10.998h-9.9982v-9.9982h9.9994v9.9982z"/></svg>';
var fill_dlzen='<svg viewBox="0 0 12 16" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m10.998 4h-9.9982c-0.55219 0-1.0003 0.44812-1.0003 1.0003v9.9994c0 0.55219 0.44812 1.0003 1.0003 1.0003h9.9994c0.55219 0 1.0003-0.44812 1.0003-1.0003v-9.9994c-0.001292-0.55219-0.44934-1.0003-1.0015-1.0003zm0 10.998h-9.9982v-9.9982h9.9994v9.9982z"/><path d="m9.0709 0-2.6896 3.4027h-1.4679l1.7634-3.4027z"/></svg>';

jQuery("ins").remove();
jQuery("body").children().hide();
jQuery(".entry-content").children().each(function(){
  if(/H2/.test(this.tagName)) {
    jQuery("body").append(this);
  }
  if(/P/.test(this.tagName)) {
    var text = jQuery(this).text();
    var words = text.split(/\s+/);
    var line = "";
    jQuery.each(words, function(i, word){
      word = word.replaceAll("i", fill);
      word = word.replaceAll("y", fill);
      word = word.replaceAll(/*"í"*/ "\u00ed", fill_dlzen);
      word = word.replaceAll(/*"ý"*/ "\u00fd", fill_dlzen);
      line+="<span>"+word+"</span> ";
    })
    jQuery("body").append("<p>"+line+"</p>");
    //console.log(jQuery(this).text())
  }
})
jQuery("#page").remove();

jQuery("h2").css({
  color: "blue",
  margin: "20px 0 10px 0",
  "font-size": "30px"
})
jQuery("p").css({
  margin: "10px",
  "font-size": "35px"
})
jQuery("span").css({
  "white-space": "nowrap"
})
